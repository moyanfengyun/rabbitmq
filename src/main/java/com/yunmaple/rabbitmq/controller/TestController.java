package com.yunmaple.rabbitmq.controller;

import com.yunmaple.rabbitmq.entity.Order;
import com.yunmaple.rabbitmq.service.MessageServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
@RequestMapping("/mq")
@Slf4j
public class TestController {

    @Autowired
    private MessageServiceImpl messageService;

    final AtomicInteger rc = new AtomicInteger();

    @GetMapping("/sendMsg")
    public String sendMsg() {
        int random=(int)(Math.random()*5+1);
        int index = rc.incrementAndGet();
        Order order = new Order();
        order.setStatus(1);
        order.setCretime(new Date());
        order.setStoptime(random);
        log.info("发送第{}条消息：{}", index,order);
        messageService.sendMsg("test_queue_1",order);
        return "success";
    }

}
