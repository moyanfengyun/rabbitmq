package com.yunmaple.rabbitmq.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@ToString
public class Order implements Serializable {
    private Integer id;//订单编号
    private Integer status;//订单状态
    private Integer stoptime;//订单延时自动取消时间
    private Date cretime;//订单创建时间
    private Date usetime;//订单自动取消时间
}
