package com.yunmaple.rabbitmq.mapper;

import com.yunmaple.rabbitmq.entity.Order;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderMapper {

    Order getOrderById(@Param("id") Integer id);

    int addOrderInfo(Order order);

    int updateOrderInfo(Order order);

}
